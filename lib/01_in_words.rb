class Fixnum
  def in_words
    @@answerstack = []
    return "zero" if self == 0
    parse_number
    handle_exceptions
    @@answerstack.reverse.join(" ")
  end
    #depending on length of number...
    def parse_number
      arr = self.to_s.split('')
      case arr.length
        when 1
        firsthundreds(arr[-1], "0", "0")
        when 2
        firsthundreds(arr[-1], arr[-2], "0")
        when 3
        firsthundreds(arr[-1], arr[-2], arr[-3])
        when 4
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], "0", "0")
        when 5
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], "0")
        when 6
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        when 7
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], "0", "0")
        when 8
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], "0")
        when 9
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], arr[-9])
        when 10
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], arr[-9])
        fourth_hundreds(arr[-10], "0", "0")
        when 11
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], arr[-9])
        fourth_hundreds(arr[-10], arr[-11], "0")
        when 12
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], arr[-9])
        fourth_hundreds(arr[-10], arr[-11], arr[-12])
        when 13
        firsthundreds(arr[-1], arr[-2], arr[-3])
        second_hundreds(arr[-4], arr[-5], arr[-6])
        third_hundreds(arr[-7], arr[-8], arr[-9])
        fourth_hundreds(arr[-10], arr[-11], arr[-12])
        fifth_hundreds(arr[-13], "0", "0")
      end

    end

    def firsthundreds(ones, tens, hundreds)

        #if we got teens we need to know
        if possibleteen?((tens+ones).to_i)
          generateteens((tens+ones).to_i)
          generatehundreds(hundreds.to_i)
        else
        generateones(ones.to_i)
        generatetens(tens.to_i * 10)
        generatehundreds(hundreds.to_i)
        end
    end

    def second_hundreds(thousands, tenk, hundredk)

        if possibleteen?((tenk+thousands).to_i)
          @@answerstack << "thousand"
          generateteens((tenk+thousands).to_i)

        else
        @@answerstack << "thousand"
        generateones(thousands.to_i) unless thousands.to_i == 0
        generatetens(tenk.to_i * 10)
        generatehundreds(hundredk.to_i)
        end

    end

    def third_hundreds(mil,tenmil,hundredmil)
      #if we got teens we need to know
        if possibleteen?((tenmil+mil).to_i)
          @@answerstack << "million"
          generateteens((tenmil+mil).to_i)


        else
        @@answerstack << "million"
        generateones(mil.to_i) unless mil.to_i == 0
        generatetens(tenmil.to_i * 10)
        generatehundreds(hundredmil.to_i)
      end
    end

    def fourth_hundreds(bill,tenbill,hundredbill)
      if possibleteen?((tenbill+bill).to_i)
          @@answerstack << "billion"
          generateteens((tenbill+bill).to_i)

        else
        @@answerstack << "billion"
        generateones(bill.to_i) unless bill.to_i == 0
        generatetens(tenbill.to_i * 10)
        generatehundreds(hundredbill.to_i)
      end
    end

    def fifth_hundreds(trill,tentrill,hundredtrill)
      if possibleteen?((tentrill+trill).to_i)
          @@answerstack << "trillion"
          generateteens((tentrill+trill).to_i)

        else
        @@answerstack << "trillion"
        generateones(trill.to_i) unless trill.to_i == 0
        generatetens(tentrill.to_i * 10)
      end
    end


    def generateones(num)
      @@answerstack << translateones(num) unless num == 0 && @@answerstack.empty?
    end

    def generatetens(num)
      @@answerstack << translatetens(num) unless num == 0
    end

    def generateteens(num)
      @@answerstack << weirdteens(num)
    end

    def generatehundreds(num)
      @@answerstack << "hundred" unless num == 0
      @@answerstack << translateones(num) unless num == 0

    end

    def translateones(num)
      onestranslator = Hash.new(0)
      zerotonine = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
      (0..9).each {|fnum| onestranslator[zerotonine[fnum]] = fnum}
      onestranslator.index(num)
    end

    def translatetens(num)
      tenstranslator = {
        "ten" => 10, "twenty" => 20, "thirty" => 30, "forty" => 40, "fifty" => 50,
        "sixty" => 60, "seventy" => 70, "eighty" => 80, "ninety" => 90 }
      tenstranslator.index(num)
    end

    def weirdteens(num)
      teentranslator = Hash.new(0)
      teens = ["eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
      (11..19).each {|fnum| teentranslator[teens[fnum-11]] = fnum}
      teentranslator.index(num)
    end

    def possibleteen?(num)
      return true if (11..19).include?(num)
      false
    end

    def handle_exceptions
    @@answerstack.reject!.each_with_index {
      |el, idx| el == "thousand" && @@answerstack[idx +1] == "million"}
    @@answerstack.reject!.each_with_index {
      |el, idx| el == "million" && @@answerstack[idx +1] == "billion"}
      @@answerstack.reject!.each_with_index {
      |el, idx| el == "billion" && @@answerstack[idx +1] == "trillion"}
    end


end
